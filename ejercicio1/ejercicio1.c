#include <stdio.h>
#include <stdlib.h>

int main (){
    float n1;
    float n2;
    float *p1;
    float *p2;
    n1 = 4.0;
    p1 = &n1;
    p2 = p1;
    n2 = *p2;
    n1 = *p1 + *p2;
    printf("el valor de n1 es: %2.2f\n", n1);
    printf("el valor de n2 es: %2.2f\n", n2);
return 0;
}
