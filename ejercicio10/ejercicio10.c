#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
	char nombre[40];
	int dui;
	float sueldo;
} empleado;

void fregistro(empleado *emp[], int i);

void orden(empleado *emp[],empleado *emp1, int i, int caso);

int main (){

 empleado *emp[1000],*emp1;

 int registro=1,i=0;

 do{
	printf("Desea ingresar Registro \n 0. NO , 1. SI\n");
	scanf("%d",&registro);

	if(registro==1 && i<1000){
	   fregistro(emp, i);
	   i++;
	}

 }while (registro==1); 

   system("clear");

// calculando promedio
 float promedio=0;

  for(int j = 0 ; j < i;j++){
	promedio= promedio + emp[j]->sueldo;
  }
  
  promedio=promedio/i;

// mostrando Valores en Pantalla
  printf("ORDENANDO POR EL NOMBRE\n");
     printf("===============================================================\n");
	 
	  orden(emp,emp1, i, 0);

	  for( int j= 0; j <i; j++){
	    printf("\tnombre: %s, \tdui: %d, \tsueldo: %.2f\n",emp[j]->nombre, emp[j]->dui, emp[j]->sueldo);
	  }

	  printf("\n\nORDENANDO POR EL SUELDO DE MAYOR A MENOR\n");
     printf("===============================================================\n");

	  orden(emp,emp1,i,1);

	  for( int j= 0; j <i; j++){
	    printf("\tnombre: %s, \tdui: %d, \tsueldo: %.2f\n",emp[j]->nombre, emp[j]->dui, emp[j]->sueldo);
	  }

	printf("\n\n El promedio es: %.2f\n",promedio);
     printf("===============================================================\n");

}

void fregistro(empleado *emp[], int i){

        emp[i] = (empleado *) malloc(sizeof(empleado));

   	 if (emp[i] == NULL){
		printf("Error en la asignacion de memoria");
		exit(1);
	 }
	system("clear");
     printf("INGRESANDO DATOS DEL EMPLEADO #%d\n",(i+1));
     printf("===============================================================\n");
	 printf("ingrese el nombre del empleado: ");
	 scanf("%s",emp[i]->nombre);

	 printf("Ingrese el numero de DUI: ");
	 scanf("%d",&emp[i]->dui);

	 printf("ingrese el sueldo del empleado: ");
	 scanf("%f",&emp[i]->sueldo);
}

void orden(empleado *emp[],empleado *emp1, int i, int caso){

	for( int j = 0; j < i; j++){

	    for( int z = 0; z< i-1; z++){

		if(caso==0){

		  if(strcasecmp(emp[z]->nombre,emp[z+1]->nombre)>0){
		     emp1 = emp[z];
		     emp[z] = emp[z+1];
		     emp[z+1] = emp1;
		  }

		}else{

		  if(((emp[z]->sueldo)-(emp[z+1]->sueldo))<0){
		     emp1 = emp[z];
		     emp[z] = emp[z+1];
		     emp[z+1] = emp1;
		  }
		}
	    }
	}
}
