#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void ingresoPalabra(char *palabras[], int palabra);
void ordenPalabras(char *palabras[], int palabra);

void main(){
  int palabra;

  printf("¿Cuantas palabras ingresara? ");
  scanf("%d",&palabra);

  char *palabras[palabra];

//ingresando las palabras
  ingresoPalabra(palabras,palabra);


//mostrando las palabras ingresadas.
  printf("Las palabras ingresadas son: ");
	for(int i = 0; i< palabra; i++){
	 printf("%s, ",palabras[i]);
	}

 printf("\n\nORDENANDO LAS PALABRAS:\n");
 printf("-------------------------------------------\n");

 ordenPalabras(palabras,palabra);

 //mostrando palabras ordenadas
	for(int i = 0; i< palabra; i++){
	 puts(palabras[i]);
	}
}


void ingresoPalabra(char *palabras[], int palabra){

  for(int i = 0;i < palabra; i++){
	printf("ingrese la %d° palabras: ",(i+1));
	palabras[i]=(char*) malloc(sizeof(char)*palabra);
	if(palabras[i]==NULL){
	 printf("Error al asignar memoria");
	 exit(1);
	}
	scanf("%s",palabras[i]);
  }
}


void ordenPalabras(char *palabras[], int palabra){
char *burbuja;
 for( int i = 0; i < palabra; i++){
    for(int j = 0; j < (palabra-1); j++){
	if((strcmp(palabras[j],palabras[j+1]))>0){
	  burbuja = palabras[j];
	  palabras[j]= palabras[j+1];
	  palabras[j+1]=burbuja;
	}
    }
 }
}


