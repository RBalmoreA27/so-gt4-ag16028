#include <stdio.h>
#include <stdlib.h>

int main (){
 int cantidad, i;
 float *num, *numeros;
 printf("Cantidad de datos a ingresar: ");
 scanf("%d",&cantidad);
 num = (float *) malloc(sizeof(float)*(cantidad+1));

 if(num == NULL){
   printf("Error en asignar memoria");
   exit(1);
 }
 
 *num = cantidad;
  numeros = num;
  numeros++;

 for (int i = 0; i < cantidad; i++){
   printf("ingrese el %d° dato: ",(i+1));
   scanf("%f",numeros++);
 }

  printf("los numeros ingresados son: ");
  numeros= &num[1];	
 for(int i = 0; i < cantidad; i++){
   printf("%4.3f,  ",*(numeros++));
 }

 float operacionNumero;
 operacionNumero = num[1];

//Calculando Valor Maximo
 for( i = 1; i < cantidad+1; i++){
    (operacionNumero < num[i])? operacionNumero = num[i]:printf("");
 }
 printf("\nEl numero maximo es: %4.3f\n",operacionNumero);

//Calculando el Valor minimo
  operacionNumero = num[1];
 for(i = 1; i < cantidad + 1; i++){
  (operacionNumero > num[i])? operacionNumero = num[i]: printf("");
 }
  printf("El numero minimo es: %4.3f\n",operacionNumero);

//Calculando la Media Aritmetica
 operacionNumero = 0;
 for (i = 1; i < cantidad + 1; i++){
  operacionNumero+=num[i];
 }
 printf("La media aritmetica es: %4.4f\n",(operacionNumero/cantidad));
 return 0;
}
