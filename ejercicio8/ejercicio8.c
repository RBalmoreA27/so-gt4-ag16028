#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

char *pedirTexto();
void contarVocales(char*, int[]);
void imprimir(int[]);

void main (){
	char *texto;
	int num[5];
	texto=pedirTexto();
	contarVocales(texto,num);
	imprimir(num);
}

char *pedirTexto(){
  char *texto;
  texto = (char*) malloc(sizeof(char)*50);

  if ( texto==NULL){
     printf("error en la asignacion de memoria");
     exit(1);
  }

  printf("ingrese una frase: ");
  scanf("%[^\n]s",texto);
  return texto;
}

void contarVocales(char *texto, int num[]){
  num[0]=0;
  num[1]=0;
  num[2]=0;
  num[3]=0;
  num[4]=0; 

  for (int s = 0; texto[s]!='\0'; s++){

     char letra=tolower(texto[s]);

     switch(letra){
   	case 'a' :
		num[0]=num[0]+1;
   	break;
  	case 'e' :
		num[1]=num[1]+1;
    	break;
   	case 'i' :
		num[2]=num[2]+1;
     	break;
   	case 'o' :
		num[3]=num[3]+1;
     	break;
   	case 'u' :
		num[4]=num[4]+1;
     	break;
   	default: 
//		printf("\no es vocal\n");
     	break;
     }
  }
}

void imprimir(int num[]){
  printf("\nImprimiendo reporte: \n");
  printf("Total de repeticiones de la vocal a es: %d\n",*num);
  printf("Total de repeticiones de la vocal e es: %d\n",*(num+1));
  printf("Total de repeticiones de la vocal i es: %d\n",*(num+2));
  printf("Total de repeticiones de la vocal o es: %d\n",*(num+3));
  printf("Total de repeticiones de la vocal u es: %d\n",*(num+4));
}
