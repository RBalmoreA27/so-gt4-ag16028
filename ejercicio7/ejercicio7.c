#include <stdio.h>
#include <stdlib.h>
double exponente(double base, double exponente);

int main (){
 int grado, i;
 double *coeficientes, *coeficiente, punto, resultado;
 
 printf("ingrese el el grado del polinomio: ");
 scanf("%d",&grado);

 coeficientes = (double*) malloc(sizeof(double)*(grado+1));
 coeficiente = coeficientes;

 for(i = 0; i < grado+1; i++){
  i>1?printf("ingrese el coeficiente de x^%d: ",i) : i==0? printf("ingrese el termino independiente: "):printf("ingrese el coeficiente de x: ");
  scanf("%lf",coeficiente++);
 }

//pidiendo el punto a evaluar
  printf("Ingrese el punto(x) que desee evaluar en el polinomio: ");
  scanf("%lf", &punto);

//evaluando el punto 
  resultado = *coeficientes;
  coeficiente = &coeficientes[1];
  for( i =0 ; i <grado; i++){
    resultado = resultado + coeficiente[i] * exponente(punto,i);
  }

//mostrando el Polinomio 
  coeficiente= &coeficientes[0];
  printf("el polinomio es: ");
  for(i = 0; i< grado +1; i++){
     if (i>0){
       ( *coeficiente>0 )? printf(" + "): printf(" ");
     }

    i>1?printf("%gX^%d ",*(coeficiente++),i): i==0? printf("%g",*coeficiente++): printf("%gX",*coeficiente++);
  }

  printf("\nEl polinomio evaluado en F(%g) es: %g\n",punto,resultado);
  return 0;
}

double exponente(double base, double exponente){
   double resultado=base;
   while (exponente>0) {
     resultado =resultado* base;
     exponente--;
   }
 return resultado;
}
