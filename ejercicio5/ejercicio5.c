#include <stdio.h>
#include <stdlib.h>

int main(){
 int *pnumero,numero=15, i, *p_pnumero;

 pnumero = (int *) malloc(sizeof(int)*numero);

 if (pnumero==NULL){
  printf("No se pudo asignar memoria... \nSaliendo...");
  exit(1); 
 }
 p_pnumero = pnumero;
 for(i = 0; i < numero; i++){
 *(p_pnumero+i) = rand()%101;
 }
 printf("los numeros son:\n");
 for( i = 0; i < numero; i++){
 printf("%d, ",*(pnumero+i));
 }

//ordenando de mayor a menor
 printf("\nImprimiendo los numeros en orden:\n");
 int burbuja;
 for(int j= 0 ; j < numero; j++){
   for (i = 0; i < numero-1; i++){
    if(p_pnumero[i] < (p_pnumero[i+1] )){
	burbuja = p_pnumero[i];
	p_pnumero[i] = p_pnumero[i+1];
	p_pnumero[i+1]= burbuja;
    }
  }
 }
 
 for( i = 0; i < numero; i++){
    printf("%d, ",*(pnumero+i));
  }
printf("\n");
 free(pnumero);
 return 0;
}

